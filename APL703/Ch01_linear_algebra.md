---
title: " Chapter 1: Linear Algebra"
numbersections: true
date: \today
author: "Prapanch Nair"
bibliography: "bibliography.bib"
link-citations: true
urlcolor: "blue"
csl: "https://raw.githubusercontent.com/citation-style-language/styles/master/harvard-anglia-ruskin-university.csl"
header-includes: |
    \usepackage{color}
    \usepackage[margin=1in]{geometry}
    \setlength{\parskip}{2pt}
    \newcommand{\bA}{\mathbf{A}}
    \newcommand{\bI}{\mathbf{I}}
    \newcommand{\bB}{\mathbf{B}}
    \newcommand{\bC}{\mathbf{C}}
    \newcommand{\bx}{\mathbf{x}}
    \newcommand{\bb}{\mathbf{b}}
    \newcommand{\bv}{\mathbf{v}}
    \newcommand{\bu}{\mathbf{u}}
    \newcommand{\be}{\mathbf{e}}
    \newcommand{\cof}{\text{cof}}
    \newcommand\textbreak{%
    \begin{center}%
    \decothreeleft \aldineleft \decosix \aldineright \decothreeright%
    \end{center}}
    \pagestyle{empty}
---
*This document is a work in progress and will be updated as the chapter's lectures progress.*

# Basics of Matrices

When solving a linear system of equations simultaneously, we encounter an array of numbers. For notational convenience, this array of numbers can be written separately. Then, perhaps, we could find important properties of the system of equations from the properties of these matrices. Take for instance, a linear system of equations: 
$$
\begin{split}
2x + 3y &= 2  \\
4x + y &= 1 
\end{split}
\label{la_example}
$$
Can be written as a matrix multiplied by a vector equals a vector, as follows:
$$
\begin{bmatrix}
2 & 3 \\ 
4 & 1 
\end{bmatrix}
\begin{bmatrix}
x \\ y
\end{bmatrix}
=\begin{bmatrix}
2 \\ 1
\end{bmatrix}
$$
which can be written shortly as 
$$
\mathbf{A} \mathbf{x} = \mathbf{b}
$$
where 
$$
\mathbf{A} = \begin{bmatrix}
2 & 3 \\ 
4 & 1 
\end{bmatrix} \qquad 
\mathbf{x} = \begin{bmatrix}
x \\ y
\end{bmatrix} \qquad
\mathbf{b} = \begin{bmatrix}
2 \\ 1
\end{bmatrix}
$$

All the three above are matrices of different forms: $\mathbf{A}$ is of form $2\times 2$, $\mathbf{x}$ and $\mathbf{b}$ are both of form $2\times 1$. A matrix $\mathbf{K}$ of form $m\times n$ has $m$ rows and $n$ columns. I have used **bold** notation for a matrix.  You can use an index notation, as well, as follows: 
$$ \mathbf{A} =  [a_{ij}] ,$$
where the subscripts tell us that there are multiple elements $a$ indicated by their $i$ and $j$ *coordinates* within the matrix. 

Now that we see that a linear system can be written as a single equation using matrices, it may be useful to list out some properties of matrices. After this, we may be able to get some physical intuition for the matrices, and their usefulness.

## Properties of matrices
A matrix $\mathbf{A}$ of form $m\times n$ may be represented as:
$$
A = \begin{bmatrix}
a_{11} & a_{12} & \ldots & a_{1n} \\
a_{21} & a_{22} & \ldots & a_{2n} \\
\vdots & \vdots & \ddots & \vdots \\
a_{m1} & a_{m2} & \ldots & a_{mn} 
\end{bmatrix}
$$
A horizontal line is called a row and a vertical one, a column. 


1. Two matrices are equal only if they are of the same form and if their corresponding elements are equal. They are unequal for any other case. For example:
$$
\begin{bmatrix}
1 \\2 
\end{bmatrix}
\neq 
\begin{bmatrix}
1 & 2 
\end{bmatrix}
$$
2. If $\mathbf{A} =[a_{ij} ]$  and $\mathbf{B}=[b_{ij}]$ are two matrices of the same form, then 
$$ \mathbf{A} + \mathbf{B} = [a_{ij} + b_{ij}]
$$ and the sum is of the same form of either $\mathbf{A}$ or $\mathbf{B}$. Two matrices of the same form are said to be *conformable*. For non-conformable matrices, addition is not defined. 
**Assignment 1:** Show that addition of matrices is commutative.
3. If $\mathbf{A} = [a_{ij}]$ is a matrix and $c$ is a scalar, their product is 
$$
c\mathbf{A} = [ca_{ij}].
$$ 
This matrix is also of the same form as $\mathbf{A}$. This automatically implies 
$$
-\mathbf{A} =(-1)\mathbf{A} .
$$ 
Also, a zero matrix is a matrix of all zero elements.  
We may now write the following properties that follow:
$$
\begin{aligned}
\bA + \bB &= \bB +\bA  &\text{commutativity} \\
(\bA + \bB) +\bC &= \bA + (\bB + \bC) &\text{associativity} \\ 
\bA + 0 &= \bA &\text{identity of addition} \\ 
\bA + (-\bA) & = 0 \\
\alpha(\beta \bA) &= (\alpha \beta )\bA &\text{associativity}\\
(\alpha+\beta) \bA &= \alpha \bA +\beta \bA &\text{distributivity}\\
\alpha(\bA + \bB) &= \alpha \bA + \alpha \bB &\text{distributivity}\\
1\bA &=\bA \\ 
0\bA &=0 \\ 
\alpha 0 &= 0
\end{aligned}
$$
**Assignment**: Prove each of the above property of matrices. 

## Matrix multiplication
Multiplication of two matrices:
$$
\bA \bB = \sum_{k=1}^n a_{ik} b_{kj} \qquad (1\leq i \leq m, \, 1\leq j \leq p)
$$

Example:
$$
\bA = \begin{bmatrix}
2 & 0 & -5 \\ 
1 & 3 & 2 \\ 
4 & 1&  -1 \\
0 & 2 & 7
\end{bmatrix} 
\qquad \& \qquad 
\bB = \begin{bmatrix}
5 & 1 \\ -2 & 3 \\  1 & 0
\end{bmatrix}
$$
We move accross the rows of the first matrix and the columns of the second  matrix. 
**Comment:** Can we see the row of the first and the column of the second as vectors? What is the actual operation that is happening here? A dot product. 
$$
\bA\bB \neq \bB\bA
$$
The above is easy to see in our example above because $\bB\bA$ cannot be defined. However, is it possible to show this when multiplication can be defined? 

Example:
$$
\bA = \begin{bmatrix}
2 & 3 \\ 
3 & 0 \\ 
\end{bmatrix}, 
\qquad  
\bB = \begin{bmatrix}
-2 & 3 \\ 3 & -4 
\end{bmatrix},
\qquad 
\bC = \begin{bmatrix}
2 & 3 \\ 1 & 0 
\end{bmatrix}
$$
Here what about commutivity? 

Because matrix multiplication (as defined by Cayley) is not commutative, we state the direction of multiplication as **pre** or **post** multiplication. Why couldn't we keep a commutative definition? Refer to the first example linear system. The current definition of multiplication is very useful in expressing linear systems. 

## Diagonal matrix

## Identity matrix
$$
\mathbf{I} = \begin{bmatrix}
1 & 0 & \ldots & 0 \\
0 & 1 & \ldots & 0 \\
\vdots & & \ddots &\vdots \\ 
0 & & \ldots &1
\end{bmatrix}  = [\delta_{ij}]
$$
**Kronecker delta** 
$$
\delta_{ij} = \begin{matrix} 
1 & \text{if} & i=j \\
0 & \text{if} & i\neq j
\end{matrix}
$$

## Multiplication properties
1. $\bA\bB \neq \bB\bA$
2. $\bA\bB = \bA\bC$ does not imply $\bB =\bC$.
3. $\bA\bB =0$ does not imply that $\bA = 0$ and/or $\bB=0$
4. $\bA^2 = \mathbf{I}$ does not imply $\bA = \mathbf{I}$.
5. $(\alpha \bA) \bB = \bA(\alpha \bB) = \alpha (\bA\bB)$
6. $\bA(\bB\bC) = (\bA\bB)\bC$
7. $(\bA+\bB)\bC = \bA\bC +\bB\bC$
8. $C(\bA+\bB) = \bC\bA+\bC\bB$
9. $A(\alpha \bB + \beta \bC) = \alpha \bA\bB +\beta \bA\bC$

**Assignment**: Prove all the above.

## Transpose matrix
$$ \bA = [a_{ji}] $$

### Transpose Properties:

1. $(\bA^T)^T = A$
2. $(\bA +\bB)^T = \bA^T + \bB^T$
3. $(\alpha \bA)^T = \alpha \bA^T$
4. $(\bA\bB)^T = \bB^T\bA^T$

**Assignment**: Prove the above. Prove 4 in class. 

## Symmetric and Skew-symmetric matrices. 
A matrix $\bA$ is symmetric if $\bA = \bA^T$ and it is called *skew-symmetric* if 
$\bA = - \bA^T$. In index notation, for a symmetric matrix,
$A_{ij} = A_{ji}$ and for a skew-symmetric matrix $A_{ij}=-A_{ji}$. The index notation
readily reveals that a skew-symmetric matrix as zero as diagonal elements (which is the only
way a number is equal to its negative). 

Any  matrix can be written as a sum of a symmetric and a skew-symmetric matrix. 
$$
\bA = \frac{\bA + \bA^T}{2} + \frac{\bA - \bA^T}{2}.
$$

## Determinants
If $\bA$ is a square matrix, its determinant is written as $\text{det} \bA$. The following needs to be remembered:
$$
\text{det}\bA =  \text{det}\bA^T \qquad \text{and} \qquad \text{det}\bA\bB =  (\text{det}\bA)(\text{det}\bB)
$$
If $\bA$ is a $N \times N$ square matrix, we can construct an $(N-1)\times (N-1)$ square matrix by removing the $i$\textsuperscript{th} row and the $j$\textsuperscript{th} column. The determinant of this matrix is denoted by $M_{ij}$ and is the minor of $A_{ij}$. The cofactor of $A_{ij}$ is defined by 
$$
\text{cof} A_{ij} = (-1)^{i+j} M_{ij}
$$
For example, for the matrix
$$
A = 
\begin{bmatrix}
A_{11} & A_{12} \\ 
A_{21} & A_{22}
\end{bmatrix}
$$
$$
M_{11} = A_{22}, \, M_{12}=A_{21} ,\, M_{21} = A_{12},\, M_{22} = A_{11} \\
\text{cof}A_{11} = A_{22},\, \text{cof}A_{12} = -A_{21},\, \text{etc.}
$$

The adjoint of a $N\times N$ matix, also written as $\text{adj}\bA$ is an $N\times N$
matrix:
$$
\text{adj}\bA = \begin{bmatrix}
\text{cof} A_{11} & \text{cof} A_{12} & \ldots & \text{cof} A_{1n} \\ 
\text{cof} A_{21} & \text{cof} A_{22} & \ldots & \text{cof} A_{2n} \\ 
\vdots & & & \\
\text{cof} A_{n1} & \text{cof} A_{n2} & \ldots & \text{cof} A_{nn} \\ 
\end{bmatrix}
$$
As an example for the $2\times 2$ matrix, 
$$
\text{adj} \bA = \begin{bmatrix}
A_{22} & -A_{12} \\
-A_{21} & A_{11}
\end{bmatrix}
$$

We will soon prove the following:
$$
A(\text{adj}A) = (\text{det} A)\textbf{I} = (\text{adj}A) A
$$
This can be easily seen in the above size 2 matrix, as follows:

$$
A(\text{adj}A) = \begin{bmatrix}
A_{11} & A_{12} \\
A_{21} & A_{22}
\end{bmatrix}
\begin{bmatrix}
A_{22} & -A_{12} \\
-A_{21} & A_{11}
\end{bmatrix}
=
\begin{bmatrix}
A_{22} A_{11} -A_{12} A_{21} & 0\\
0  & A_{22} A_{11} -A_{12} A_{21} 
\end{bmatrix}
$$
Thus, 
$$
A(\text{adj}A) =  (A_{22} A_{11} -A_{12} A_{21}) \textbf{I} = (\text{det})\textbf{I} 
$$

Similarly 
$$
(\text{adj}\bA)\bA =  (\text{det})\textbf{I} 
$$
If $\bA$ has a non zero determinant, then we can use the above result to define 
and inverse operation for matrix multiplication as follows:
$$
\bA^{-1} = \frac{\text{adj} \bA}{\text{det}\bA}
$$
We can use the earlier result that determinant of a product of two matrices is
equal to the product of the determinant. If $\bA^{-1}$ exists, then $(\text{det}\bA)(\text{det}\bA^{-1}) = \text{det}\bI = 1 \neq 0$.
So a non-zero determinant implies a non-singular matrix. 

Matrices are convenient for representing linear system of equations like:
$$
\begin{split}
A_{11}x_1 + A_{12}x_2 + \ldots + A_{1n}x_n = b_1 \\
A_{21}x_1 + A_{22}x_2 + \ldots + A_{2n}x_n = b_2 \\
\vdots \\
A_{n1}x_1 + A_{n2}x_2 + \ldots + A_{nn}x_n = b_n 
\end{split}
$$

This can be written in the form of matrices as:

$$
A = \begin{bmatrix}
A_{11} & A_{12} & \ldots & A_{1n} \\
A_{21} & A_{22} & \ldots & A_{2n} \\
\vdots & \vdots & \ddots & \vdots \\
A_{n1} & A_{n2} & \ldots & A_{nn} 
\end{bmatrix}
\begin{bmatrix}
x_1 \\
x_2 \\
\ldots \\
x_n
\end{bmatrix}
=
\begin{bmatrix}
b_1 \\
b_2 \\
\ldots \\
b_n
\end{bmatrix}
$$

Which can be written using the earlier bold notation for matrices as:
$$
\bA \mathbf{x} = \mathbf{b}
$$
Now, if $\bA$ is non-singular (which means if a non zero determinant exists),
then 
$$ \mathbf{x}  =  \bA^{-1} \mathbf{b} $$.
For $n=2$, we can write
$$\begin{bmatrix}
x_1 \\ x_2
\end{bmatrix} 
= \frac{1}{\text{det} \bA} \begin{bmatrix}
A_{22} & -A_{12} \\
-A_{21} & A_{11} 
\end{bmatrix}
\begin{bmatrix}
b_1 \\ b_2 
\end{bmatrix}
$$

What would happen if the matrix is singular? One can clearly see that the above
set of equations can't be solved in a straightforward manner. But can there be 
a solution at all? Can there be multiple solutions? In the case of 2 dimensions we 
can check these using simple geometric examples involving equations of straight 
lines. But what if the system of equations is larger in number? It would be clearly 
difficult to imagine the problem geometrically. In that case we would need a tool
that allows us to imagine and formulate linear relationships in a n-dimensional space. 
Such a tool is called a vector space. 

# Vector spaces

It may be a good idea to understand vector spaces as a fundamental mathematical 
idea. The reason we do this is so that it will help to see a "big-picture". 
When solving problems such a big picture understanding helps us see where to place 
a problem and what tool to use. So let's make a brief foray into the foundations
of a vector space. 
What is introduced in this lecture will not be part of any evaluation, and is 
only for some inspiration.

## Basic set theory

Set is a collection of objects. In a collection the order or arragement of elements don't matter.
When a set $\mathcal{A}$ is made of elementes $a$ then we denote this as:
$$ \mathcal{A} = \{ a | P(a) \} $$
Where $P(a)$ is a property that $a$ satisfies. 

If $\mathcal{A}$ and $\mathcal{B}$ are sets where the elements of $\mathcal{B}$
are all present in $\mathcal{A}$ then we say $\mathcal{B}$ is a subset of $\mathcal{A}$
and is denoted as $\mathcal{B} \subset \mathcal{A}$. 

Now recall the concept of a *null set*, *singleton set*, *a class*, *a proper subset*, 
*Union*:

$$ \mathcal{A} \cup \mathcal{B} = \{ x | x\in \mathcal{A} \text{ or }  x\in \mathcal{B} \} $$

This operation is commutative and associative. There is also the *intersection* of 
sets:

$$ \mathcal{A} \cap \mathcal{B} = \{ x | x\in \mathcal{A} \text{ and } x\in \mathcal{B} \} $$

Recall commutativity, associativity and disjoint sets. Union and intersection are 
distributive.
Complement of a set $\mathcal{B}$ with respect to $\mathcal{A}$ is the set of elements
present in $\mathcal{A}$ but not in $\mathcal{B}$, denoted by 
$$ \mathcal{A} / \mathcal{B} = \{ x | x\in \mathcal{A} \text{ and } x \notin \mathcal{B} \}
$$
A set doesn't say anything about the order of its elements. An ordered pair on 
the other hand emphasises the order of the elements. The ordered pair of elements
$(a,b)$ can be written in the form of a class of sets like this: $\{ \{a\}, \{a, b\}\}$. 
For $(b,a)$ this would be 
$\{ \{b\}, \{b, a\}\}$.

We can establish equality of ordered pairs as equality of its elements. 
The definition of ordered pairs can be extended to N-tuples. 

*Cartesian product* of two sets $\mathcal{A}$ and $\mathcal{B}$ is:
$$
\mathcal{A} \times \mathcal{B} = \{ (a,b) | a\in \mathcal{A}, b\in \mathcal{B} \}
$$
A subset $\mathcal{K}$ of $\mathcal{A} \times \mathcal{B}$ is called a relation
from set $\mathcal{A}$ to $\mathcal{B}$.

## Function
A relation from $\mathcal{X}$ to $\mathcal{Y}$ is called a *function* (or a mapping)
if $(x,y_1) \in f$ and $(x,y_2) \in f$ imply $y_1 = y_2$ for all $x\in \mathcal{X}$. Thus
  a function is a particular type of a relation such that for each $x\in \mathcal{X}$
  there exists one and only one $y \in \mathcal{Y}$ such that $(x,y)\in f$. Thus 
  a function $f$ defines a *single-valued* relation. For a function, we have the 
  following notation:
  $$ 
  f: \mathcal{X} \rightarrow \mathcal{Y}
  $$
  or 
  $$
  y = f(x)
  $$
  which indicates the element $y\in\mathcal{Y}$ that $f$ associates with $x\in \mathcal{X}$.
  The element $y$ is called the value of the function at $x$.

  * Domain of $f$ is $\mathcal{X}$
  * Range of $f$ is $\mathcal{Y}$: set of all $y$ for which there exists an $f$
  such that $y=f(x)$. This can be denoted by $f(\mathcal{X})$.
  
  Example: When the domain of of $f$ is the set of all real numbers $\mathcal{R}$,
  $f$ is said to be a function of a real variable. And when the range $f(\mathcal{X})$
  also belongs to $\mathcal{R}$, then we say $f$ is a real valued function. As an 
  exception a function of a real variable need not be a real valued function. 

  If $\mathcal{X}_0$ is a subset of $\mathcal{X}$ and $f:\mathcal{X}\rightarrow\mathcal{Y}$,
  then the image of $\mathcal{X}_0$ under $f$ is the set:
  $$
  f(\mathcal{X}_0) = \{ f(x) |  x \in \mathcal{X}_0 \}
  $$
  It can be easily shown that 
  $$
  f(\mathcal{X}_0) \subset f(\mathcal{X})
  $$
 Similarly if $\mathcal{Y_0} \subset \mathcal{Y}$, then the preimage of $\mathcal{Y}_0$ 
 under $f$ is the set: 
 $$
 f^{-1}(\mathcal{Y}_0) = \{ x | f(x) \in \mathcal{Y}_0\}
 $$
 and it can be shown that 
 $$
 f^{-1}(\mathcal{Y}_0) \subset \mathcal{X}
 $$

 Different kinds of functions: $f$ is 

*  *into* $\mathcal{Y}$ if $f(\mathcal{X})$ is a proper subset of $\mathcal{Y}$.
* *onto* $\mathcal{Y}$ if $f(\mathcal{X}=\mathcal{Y}$. Also called *surjective*. 
In other words a function is onto if for every $y\in \mathcal{Y}$ there exists a 
$x\in\mathcal{X}$ such that $y=f(x)$.
* *one-to-one* (or injective) if for every $x_1,x_2\in \mathcal{X}$
$$
f(x_1) = f(x_2) \implies x_1 = x_2
$$
In other words for every element $y\in f(\mathcal{X})$ there exists only one element
 $x \in \mathcal{X}$ such that $y=f(x)$
* *one-to-one correspondence* or bijective, if $f(\mathcal{X}) =\mathcal{Y}$, and if $f$ is one-to-one.
Note that a function can be onto without being one-to-one or be one-to-one without being one. 

Examples:

1. $f(x) = x^5$ as a real valued function of a real variable. This is one-to-one because
$x^5 = z^5$ implies $x=z$ when $x,z\in \mathcal{R}$ and is also onto because for every $y\in\mathcal{R}$ 
there is exists an $x$ such that $y=x^5$.
2. $f(x) = x^3$ as a complex valued function of a complex variable. It is onto,
but not one to one. Why? $$
f(1) = f\left(-\frac{1}{2} + i\frac{\sqrt{3}}{2} \right) = f\left(-\frac{1}{2} - i\frac{\sqrt{3}}{2} \right) = 1^3
$$
3. $f(x) = 2|x|$ is into and not one-to-one. 

If $f:\mathcal{X}\rightarrow\mathcal{Y}$ is a one-to-one function, then there exists
a $f^{-1}:f(\mathcal{X}) \rightarrow \mathcal{X}$ which maps for every $y\in f(\mathcal{X})$ 
the unique $x\in\mathcal{X}$ such that $y=f(x)$. Thus,
$$
x = f^{-1}(y)
$$
is the inverse function. Unlike preimage, inverse can be defined only for one-to-one functions. 
Also $f^{-1}(f(\mathcal{X}) = \mathcal{X}$.

Now that we have defined a function, we could combine functions using a composition of 
functions. Given $f:\mathcal{X}\rightarrow \mathcal{Y}$ and $g:\mathcal{Y}\rightarrow\mathcal{Z}$ 
we can define $h:\mathcal{X}\rightarrow \mathcal{Z}$ as a composition of $f$ and $g$ denoted as
$$ h = f\circ g .$$ 
Composition may not be commutative and also that when $f\circ g$ is defined, 
$g\circ f$ may not even be defined. The operation is associative:
$$ f\circ (h\circ g) =  (f\circ h)\circ g $$
when each composition is defined. 
Identity function is a composition of a function and its inverse. 

## Groups, rings and fields

A group can be defined through a binary operation. 
If $\mathcal{G}$ is a non-empty set, a binary operation on $\mathcal{G}$ is
a function from $\mathcal{G}\times\mathcal{G}$ to $\mathcal{G}$. If we denote the
binary operation by $*$, then if $a,b\in\mathcal{G}$ then $a*b\in\mathcal{G}$. The binary 
operation on $*$ is associative. 
The binary operation can be commutative. 
A semigroup is a pair $(\mathcal{G},*)$. An element $e$ such that:
\begin{equation}
e*a = a*e = a, \qquad \text{for all } a\in\mathcal{G}
\end{equation}
is an identity element. So the identity element of multiplication is 1 and that of 
addition is 0. 
Can you prove that $\mathcal{G}$ has at the most one identity element? 
Similarly we can define inverse element. 

A group is a pair $(\mathcal{G},*)$ consisting of an associative binary operation
$*$ and a set $\mathcal{G}$ which contains the identity element and inverses of all
elements of $\mathcal{G}$ with respect to the binary operation. If the binary operation is 
commutative, the group is called a commutative group or an Abelian group. 

The set $\mathcal{R}/\{0\}$ with the binary operation of multiplication is a group. 

The set $\mathcal{R}$ with the binary operation of addition is also a group. 

The set of positive integers with multiplication can only for a semigroup. Why?

Basic properties of a group:

1. Identity element is unique. 
2. The inverse element is unique. If $n$ is a positive integer, for $a\in\mathcal{G}$
    1. For $n=1$, $a^n = a$ 
    2. For $n>1$, $a^n = a^{n-1}*a$ 
    3. $a^0 = e$, the identity element. 
    4. $a^{-n} = (a^-1)^{n}$
3. If $m,n,k$ are integers, then $a^m*a^n =a^{m+n},\qquad (a^m)^n = a^{mn},\qquad (a^{m+n})^k = a^{km+kn}$
4. $(a^{-1})^{-1} = a$ 
5. $(A*b)^{-1} = b^{-1}*a^{-1}$
6. Given two equations $x*a=b$ and $a*y = b$, unique solutions can be obtained as $x=b*a^{-1}$ and $y=a^{-1}*b$
7. For any three elements $a,b,c$ either $a*c=b*c$ or $c*a = c*b$ implies that $a=b$. 
8. For any two elements $a,b$ either $a*b = a$ or $b*a=a$ implies that $b$ is an identity element.
A non-empty subset $\mathcal{G'}$ of $\mathcal{G}$ is called a subgroup of $\mathcal{G}$ if $\mathcal{G'}$ is a group with respect to the binary operation of $\mathcal{G}$, i.e., $\mathcal{G'}$ is a subgroup of $\mathcal{G}$ if and only if (i) $e\in\mathcal{G'}$ (ii) $a\in\mathcal{G'} \implies a^{-1}\in\mathcal{G'}$ (iii) $a,b \in \mathcal{G'} \implies a*b \in \mathcal{G'}$ .
9. Let $\mathcal{G}'$ be a non empty subset of $\mathcal{G}$. Then $\mathcal{G}'$ is a subgroup if
$a,b\in\mathcal{G}' \implies a*b^{-1}\in \mathcal{G}'$.

**Proof**. To prove this we need to show that if the above condition is satisfied then the 3 conditions in point 8 above are all met. 
    (i) Since $\mathcal{G'}$ is a non-empty set, it must contain an element $a$, and consider another element, also $a$, and if that implies $a*a^{-1}\in\mathcal{G'}$, i.e, $e\in\mathcal{G'}$, which satisfies i) in 8. (ii) if $b\in\mathcal{G'}$, and since $e$ is already in $\mathcal{G'}$, and since $b^{-1}$ is in $\mathcal{G'}$ because it is a group, then $e*b^{-1}=b^{-1}\in\mathcal{G'}$. (iii) If $a,b\in \mathcal{G'}$, then $a*(b^{-1})^{-1}= a*b \in\mathcal{G'}$.  

If $\mathcal{G}$ is a group, then it is itself a subgroup of $\mathcal{G}$, and the group consisting only of the element $e$ is also a subgroup of $\mathcal{G}$. A subgroup of $\mathcal{G}$ other than $\mathcal{G}$ itself and the group $e$ is called a *proper subgroup* of $\mathcal{G}$.
10. The intersection of any two subgroups of a group remains a subgroup. 


## Rings and  Fields 

A ring is a triple $(\mathcal{D}, +,\cdot )$ consisting of a set $\mathcal{D}$ and two binary operations $+$ and $\cdot$ such that 

1. $\mathcal{D}$ with the operation $+$ is an Abelian group. 
2. The operation $\cdot$ is associative
3. $\mathcal{D}$ contains an identity element, denoted by 1, with respect to the operation $\cdot$ $$ 1\cdot a = a\cdot 1 = a $$, for all $a\in\mathcal{D}$.
4. The operations $+$ and $\cdot$ satisfy the distributive axioms
$$ a\cdot (b+c) = a\cdot b + a\cdot c \\
(b+c) \cdot a  = b\cdot a + c\cdot a
$$

The following are familiar systems that can be examples of rings:

* I with addition and multiplication

**Incomplete here, will be filled later**


**Theorem:** Any two maximal, linearly independent sets of finite-dimensional vector spaces must contain exactly the same number of vectors. 
*Proof*: Let $\{v_1,\ldots,v_N\}$ and $\{u_1,\ldots ,u_M\}$ be two maximal, linearly independent sets of $\mathcal{V}$. Then we must show that $N=M$. Suppose $N\neq M$, say $N<M$. By the fact that $\{\bv_1,\ldots ,\bv_N\}$ is maximal, the sets 
$\{\bv_1,\ldots,\bv_N, \bu_1\},\ldots,\{\bv_1,\ldots ,\bv_N,\bu_M\}$ are all linearly dependent. Hence there exists relations

$$
\begin{split}
\lambda_{11}\bv_1 + \ldots +\lambda_{1N}\bv_N +\mu_1 \bu_1 = 0 \\
\vdots \\
\lambda_{M1}\bv_1 + \ldots +\lambda_{MN}\bv_N +\mu_M \bu_M = 0 
\end{split}
$$
 where the coefficients of each equation are not all equal to zero. In fact, the coefficients $\mu_1,\dots, \mu_M$ of the vectors $\bu_1, \ldots, \bu_M$ are all nonzero, for if $\mu_i=0$ for any $i$, then
 $$
 \lambda_{i1}\bv_1 + \ldots + \lambda_{iN}\bv_N = 0
 $$

 for some non-zero $\lambda_{i1},\ldots,\lambda_{iN}$ contradicting the assumption that $\{\bv_1,\bv_2,\ldots, \bv_N$ is linearly independent. Hence we can solver for $\{ \bu_1,\ldots, \bu_M\}$ in terms of the vectors $\{ \bv_1, \ldots ,\bv_N\}$, obtaining

\begin{align}
\bu_1 = \mu_{11}\bv_1 + \ldots +\mu_{1N}\bv_N  \\
\vdots \\
\bu_M = \mu_{M1}\bv_1 + \ldots +\mu_{MN}\bv_N 
\label{ueqn}
\end{align}
where $\mu_{ij} = -\lambda_{ij} /\mu_i$ for $i=1,\ldots,M; j =1,\ldots,N$

We can now claim that the first $N$ equations in the above system of linear equations can be inverted such that the vectors $\{\bv_1,\ldots,\bv_N\}$ are given by linear combinations of the vectors $\{\bu_1,\ldots,\bu_N\}$. This inversion is possible if the coefficient matrix $[\mu_{ij}]$ for $i,j =1,\ldots N$ is non-singular. 

Lets see if we can establish that $[\mu_{ij}]$ is indeed indeed non-singular.
If suppose $[\mu_{ij}]$ is singular, then the linear system 
$$ \Sigma_{i=1}^{N} \alpha_i \mu_{ij} =0, \qquad j = 1,\ldots, N $$ would have non-trivial (not all zero) solutions 

Using the same non trivial set of $\alpha+_i$'s on the left hand side of the previous equation for $\bu$'s, we would have 
$$
\Sigma_{i=1}^{N} \alpha_i \bu_i = \Sigma_{j=1}^{N} \left( \Sigma_{i=1}^N \alpha_i \mu_ij \right) \bv_j = \mathbf{0} 
$$

which contradicts the assumption that the set $\{ \bu_1,\ldots,\bu_N\}$, being a subset of a linearly independent set $\{ \bu_1,\ldots, \bu_M\}$, is linearly independent. 
So clearly 
$[\mu_{ij}]$ is invertible and then we can get
$$
\begin{split}
\bv_1  =\xi_{11} \bu_1 + \ldots + \xi_{1N}\bu_N \\
\vdots \\
\bv_N  =\xi_{N1} \bu_1 + \ldots + \xi_{NN}\bu_N \\
\end{split}
$$

where $[\xi_{ij}] = [\mu_{ij}]^{-1}$ . We could then substitute for $\bv$s in the equation for $\bu_{N+1}\ldots \bu_{M}$, 
$$
\begin{split}
\bu_{N+1} = \Sigma_{j=1}^{N} \left( \Sigma_{i=1}^N \mu_{N+1j} \xi_{ji} \right) \bu_i \\ 
\vdots 
\bu_M = \Sigma_{j=1}^{N} \left( \Sigma_{i=1}^N \mu_{Mj} \xi_{ji} \right) \bu_i 
\end{split}
$$

This means that $\bu$'s can be written as linear combination of $\bu$'s which contradicts the assumption that $\bu$'s are linearly independent. Hence $M>N$ is impossible. 

The following corollary theory is important. 

**Theorem:** Let $\{\bu_1, \ldots, \bu_N\}$ be a maximal LI set in $\mathcal{V}$ , and suppose $\{\bv_1,\ldots, \bv_N\}$ is given as a linear combination of $\{\bu_1,\ldots,\bu_N\}$. Then $\bv$'s is also maximal, LI set if and only if the coefficient matrix $[\xi_{ij}]$ is nonsingular. 

From the preceding theorems we see that the number $N$ of vector in a maximal, linearly independent set in a finite dimensional vector space $\mathcal{V}$ is a property of $\mathcal{V}$, called the dimension of the vector space. Any maximal linearly independent set of $\mathcal{V}$ is called a *basis* of that space. From one set of bases another set can be obtained. 

### Examples

1. Canonical basis in $N$ space
2. basis in a set of 2d square matrices. 
3. 1 and $i$ forms the basis of vector pace of complex numbers. 

**Theorem** If $\{\be_1,\ldots,\be_N\}$ is a basis for $\mathcal{V}$, then every vector in $\mathcal{V}$ has the representation 
$$
\bv = \Sigma_{j=1}{N}\xi^j \be_j
$$ 

**Theorem** The scalars $\xi^j$ are unique 

Every vector in $\mathcal{V}$ can be written as 
$$
\bv = \Sigma_{j=1}{N}\lambda^j b_j
$$
Sucha a set is a generating set of $\mathcal{V}$

**Theorem** Let $\mathcal{B}=\{\bb_1,\ldots,\bb_M\}$ be a finite subset of a finite dimensional vector space $\mathcal{V}$. Then the following are equivalent:
1. $\mathcal{B}$ is a maximal linearly independent set. 
2. $\mathcal{B}$ is a linarly indepenedent generating set.
3. $\mathcal{B}$ is a minimal generating set. 



## Linear Transformations 

A special class of functions defined on the vector space, called linear transformations will now be studied. 
$$ \bA(\bu + \bv) = \bA \bu + \bA \bv \\
 \bA(\lambda \bu ) = \lambda\bA \bu 
 $$
which implies 
$$
\bA (\lambda \bu + \mu \bv ) = \lambda \bA (\bu ) + \mu \bA (\lambda \bv )
$$

If the vectors $\bv_1 , \ldots, \bv_R$ are linearly independent, then their image set $\{\bA\bv_1,\ldots,\bA\bv_R\}$ may or may not be linearly independent. $\bA$ might even map all vectors into $\mathbf{0}$.

The kernel of a linear transformation $\bA:\mathcal{V} \rightarrow \mathcal{U}$ is the set 
$$
K(\bA) = \{ \bv | \bA\bv = \mathbf{0} \}
$$

So $K$ is the pre-image of the set $\{ \mathbf{0} \}$ in $\mathcal{U}$. Also $K$ is a subspace of $\mathcal{V}$

*Proof*: Since $K$ is a subgroup, we only need to prove $\lambda \bv \in K(\bA )$ for all $\lambda \in \mathcal{R}$. 

$K$ is also called a null space. Nullity is the dimension of the kernel. 

For a linear transformation we denote the range of $\bA$ by 
$$
R(\bA) = \{ \bA \bv | \bv \in\mathcal{V}\}
$$
which is also a subspace of $\mathcal{U}$. 


$$
dim \, R(\bA) \leq min(dim\, \mathcal{V} , dim\, \mathcal{U})
$$

and 
$$
dim\mathcal{V} = dim R(\bA) + dim K(\bA)
$$

### Rank
(Greenberg) 

But this is the case because if the matrix was singular



## The Eigenvalue problem 

Many systems which are governed by equations can be analysed by looking out for persistent behavior of variables. You will soon see that such analysis will lead to a peculiar equation:
$$
\bA \mathbf{x} = \lambda \mathbf{x}
$$
Where $A$ is a $n\times n$ matrix, $\mathbf{x}$ is an unknown vector ( $n\times 1$) and $\lambda$ is an *unknown* scala. If we subtract $\lambda \mathbf{I}\mathbf{x}$ from both sides of the above equation, we get 
$$ 
(\bA  - \lambda \bI )\mathbf{x} = 0
$$
which is a homogenous system of $n$ equations in the $n$ unknowns with an additional unknown of $\lambda$. If $(\bA-\lambda \mathbf{I})$ is full ranked, then clealry $\mathbf{x}=0$ is the solution for this system, but this solution is a trivial one. But our search is for non-trivial solutions to this problem. And this is possible only if 
$(\bA-\lambda \mathbf{I})$ is rank deficient. And the non trivial solutions could exist only in the null space of this matrix. To make this matrix rank-deficient, appropriate values of $\lambda$ are needed. In fact, the following condition needs to be satisfied:
$$ 
\text{det}(\bA - \lambda \mathbf{I}) = 0 
$$
It turns out the above equation will result in a polynomial equation in $\lambda$ 's and its solutions are called the eigenvalues. For each of these eigenvalues a corresponding vector $\mathbf{x}$ can be found in the null space of $A-\lambda \mathbf{I}$, and these vectors are called the eigenvectors of $\bA$.  

The equation $\text{det}(\bA - \lambda \mathbf{I}) = 0$ is called the characteristic equation whose LHS is a polynomial of degree $n$ and will have $n$ roots in the complex plane. One or more these roots can be repeated and so we can say that the matrix will have atleast one eigenvalue and at the most $n$ distinct eigenvalues for a matrix $\bA$ of form $n\times n$. For $n=2$ we could use formulae for quadratic equations, for larger equations we could resort to the computer. For each solution $\lambda_1$ we could find the eigenvector by solution using Gauss elimination.

#### Example
$$ A = \begin{bmatrix}
2 & 2 & 1\\
1 & 3  & 1\\
1 & 2 & 2
\end{bmatrix}
$$
$$
\text{det}(\bA - \lambda \mathbf{I}) = \left|
\begin{matrix}
2-\lambda & 2 & 1 \\ 
1 & 3-\lambda  & 1 \\
1 & 2 & 2-\lambda 
\end{matrix}
\right|
= \lambda^3 - 7 \lambda^2 + 11\lambda -5 =  (\lambda -5)(\lambda -1)^2 = 0.
$$ 
The eigenvalues are $\lambda_1 = 5$ and $\lambda_2=1$ (repeated, also called as multiplicity 2). 
Now let's find the eigenspaces. 
For $\lambda_1=5$,
$$
\begin{bmatrix}
2-5 & 2 & 1 \\
1 & 3-5 & 1 \\
1 & 2 & 2-5
\end{bmatrix}
\begin{bmatrix}
x_1 \\
x_2 \\
x_3
\end{bmatrix} =
\begin{bmatrix}
0 \\
0 \\
0
\end{bmatrix}
$$

After Gauss elimination, we get 
$$
\begin{bmatrix}
-3 & 2 & 1 \\
0 & 1 & -1 \\
0 & 0 & 0 
\end{bmatrix}
\begin{bmatrix}
x_1 \\
x_2 \\
x_3
\end{bmatrix} =
\begin{bmatrix}
0 \\
0 \\
0
\end{bmatrix}
$$

For arbitrary $x_3 = \alpha$ as a solution, we get 
$$
\mathbf{e} = \alpha
\begin{bmatrix}
1 \\
1 \\
1
\end{bmatrix}
$$
The eigenspace corresponding to the $\lambda_1$ eigenvalue (5) is span $\{[1,1,1]^{T}\}$, which is a 1D subspace of $\mathcal{R}$.
For $\lambda_2=1$, after Gauss elimination,
$$
\begin{bmatrix}
1 & 2 & 1 \\
0 & 0 & 0 \\
0 & 0 & 0 
\end{bmatrix}
\begin{bmatrix}
x_1 \\
x_2 \\
x_3
\end{bmatrix} =
\begin{bmatrix}
0 \\
0 \\
0
\end{bmatrix}
$$
Introducing arbitrary $x_3= \beta$, $x_2=\gamma$ , we get 
$$
\mathbf{e} = \begin{bmatrix}
-\beta -2\gamma \\
\gamma \\
\beta
\end{bmatrix} = \beta
\begin{bmatrix}
-1 \\
0 \\
1
\end{bmatrix}
+\gamma \begin{bmatrix}
-2 \\
1 \\
0
\end{bmatrix}
$$


Thus the eigenspace corresponding to $\lambda_2 =1$ is span$\{[-1,0,1]^T,[-2,1,0]^T\}$, which is a 2D subspace of $\mathcal{R}^3$. Now, what is the equation of this plane? 

All eigenvectors can be detemined upto an arbitrary scale factor alone. Because $Ae=\lambda e$ should be valid for any scalar multiple of $e$ also. 

The rank of $A-\lambda_1 I$ is 2, hence its nullity is 1 and the eigenspace $e_1$ is one dimensional. Similarly, rank of $A-\lambda_2 I$ is 1, and so nullity is 2, and the eigenspace is 2D. But there is no reason to belive that the multiplicity of an eigenvalue necessarily equals the dimension of the corresponding eigenspace, even though it is true in this example. 
For example, 
$$
A = \begin{bmatrix}
1 & 0 & 1 \\
1 & 1 & 0 \\
0& 0 & 1 
\end{bmatrix}
$$
has eigenvalue of 1 with multiplicity 3, and yet the eigen value is 
$$
e = \alpha\begin{bmatrix}
0 \\  1 \\  0
\end{bmatrix}
$$
### Applications 
ODEs:

$$
\begin{split}
x' &= x +4y, \\ 
y' &= x + y
\end{split}
$$
on $x(t)$ and $y(t)$. There are many methods to solve them. But there we pursue a method that gives eigen value problem. 
Since the equation is linear, const-coefficient, and homogenous, we can find exponential solutions. Thus seek $x, y$ in the form 
$$
x(t) = q_1 e^{rt}, \qquad y(t) = q_2 e^{rt}
$$
substituting, we get 
$$
\begin{bmatrix}
1 & 4 \\ 1  & 1 
\end{bmatrix}
\begin{bmatrix}
q_1 \\ q_2 
\end{bmatrix}
=
r \begin{bmatrix}
q_1 \\ q_2 \end{bmatrix}
$$

which is $$
\bA \mathbf{q} = r\mathbf{q}
$$
an eigenvalue problem with $\lambda = r$. 
We are not interested in the trivial solution $q=0$, because it gives the trivial particular solution $x(t) = y(t)= 0$, whereas we seek the general solution. Proceeding as above, we obtain the eigenvalues and spaces.
$$
\lambda_1=3, \mathbf{e_1}=\alpha \begin{bmatrix}2 \\ 1 \end{bmatrix};
\qquad 
]lambda_2=-1, e_2 = \beta \begin{bmatrix} -2 \\ 1 \end{bmatrix}
$$
Each eigen pairs now gives a solution as 
$$
x(t) = \alpha\begin{bmatrix}2 \\ 1\end{bmatrix} e^{3t} ;\qquad x(t) = \beta\begin{bmatrix} -2 \\1 \end{bmatrix} e^{-t}. 
$$
We can add them/superimpose to get the general solution. 
$$
\mathbf{x}(t) = \alpha\begin{bmatrix}2 \\ 1\end{bmatrix} e^{3t} + \beta\begin{bmatrix} -2 \\1 \end{bmatrix} e^{-t}. 
$$

Split it as scalar equations ... fill .

The $\alpha$ and $\beta$ will be the integration constants and are related to the boundary conditions of the system. 

We use $q_1$ and $q_2$ in the solution forms. Why not use different exponents too? **Assignment**

This method will fail to produce a general solution if A has a repeated eigenvalue (multiplicity k) and the corresponding eigenspace has dimension less than k. **Assignment**

#### Example
Markov process. Population exchange 
 
 Run, sleep, eat 
 $$
 \begin{split}
 x_{n+1} &= x_n - (0.2+0.08)x_n +0.12 y_n + 0.1z_n \\
 y_{n+1} &= y_n - (0.2)x_n +(0.12+0.1) y_n + 0.1z_n \\
 z_{n+1} &= z_n - (0.08)x_n +0.1 y_n + 0.1z_n 
\end{split}
$$
or 
$$ 
p_{n+1} = Ap_n
$$
where $p_n = [x_n,y_n,z_n]^T$ is the population vector. 

1. Find the population $p_n$ at a discrete time. 
$$ p_n = A^np_0
$$
Does the population reach a steady state? 
$$p_{n+1} =  p_n = P$$
or $$ AP = P$$
Question is whether such non trivial P's exist. 
The above is an eigenvalue problem. 
$$
\lambda_1 = 1, lambda_2 = 0.77, lambda_3=0.60 $$ . So there is an equilibrium population vector P given by the corresponding eigenvector
$$
P = \alpha \begin{bmatrix} 0.56 \\0.62 \\0.82 \end{bmatrix}
$$
We can check for stability of the population as well. 
We can expand the initial vector in terms of eigen vectors as basis. 
$$
\begin{split}
p_0  &= c_1 e_1 +c_2 e_2 +c_3e_3 \\
p_n &= Ap_{n-1} = c_1 \lambda_1^n e_1 + c_2 \lambda_2^n e_2 +c_3 \lambda_3^ne_3 \\
& \approx c_1e_1
\end{split}
$$
The population reaches some steady state, given some conservation law can be applied. 


The above problem is called Markov chain. with markov matrix. Elements of each column sum to unity. Lamda 1 is an eigenvalue of every Markov matrix. 


### Symmetric matrices. 
Symmetric matrices occur commonly. 

#### Eigenvalue problem 

three important results
1. If $A$ is symmetric then all of its eigenvalues are real. 
2. If $\lambda$ of $A$ has multiplicity k, then the eigenspace is of dimension k. 
3. Eigenvectors corresponding to distinct eigenvalues are orthogonal. 

proof: pre and post dot product by e_k and e_j respectively for j and k eigenvalues and then use $(AB)^T = B^TA^T$

For the matrix 
$$
A = \begin{bmatrix}
2 & 1 & 1 \\
1 &2 & 1 \\ 
1 & 1 & 2
\end{bmatrix}
$$

Theorem: if a $nxn$ A is symmetrc, its eigenvector provide an orthogonal basis. 

Proof: if distinct, it is straightforward. For repeated eigen values we show the eigen space forms a vector space of dimension k.. etc.


Spring mass

$$
m_1 x_1'' +  (k_1+k_{12}) x_1 - k_{12}x_2 =  f_1(t) \\
m_1 x_1'' +  (k_1+k_{12}) x_1 - k_{12}x_2 =  f_1(t) \\
$$



[@greenberg;@strang2019linear;@bowen2008introduction]

# References
 
