---
title: "Grading Policy"
date: \today
author: "Prapanch Nair"
bibliography: "bibliography.bib"
link-citations: true
urlcolor: "blue"
csl: "https://raw.githubusercontent.com/citation-style-language/styles/master/harvard-anglia-ruskin-university.csl"
header-includes: |
    \usepackage[margin=1in]{geometry}
    \setlength{\parskip}{2pt}
    \newcommand\textbreak{%
    \begin{center}%
    \decothreeleft \aldineleft \decosix \aldineright \decothreeright%
    \end{center}}
    \pagestyle{empty}
---
## APL703: Engineering mathematics and computations


# Course Plan

| Topic                          | % of Lectures* | No. of Lectures | No. of assignments | Projects | 
| ------------------------------ | -------------- | --------------- | ------------------ | -------- | 
| Linear Algebra:                | 30%            | 7               | 2                  | 1        | 
| Differential Equations         | 15%            | 4               | 1                  | 1        | 
| Partial Differential Equations | 40%            | 8               | 2                  | 1        | 
| Multivariate calculus          |                | 3               | 1                  | 1        | 
| Transforms                     | 10%            | 4               | 1                  | 1        | 
| probability                    | 5%             | 2               | 1                  |          | 
| Total                          |                | 28              | 8                  |          | 
|                                |                |                 |                    |          | 
\*Subject to change.

## Grading Policy
Distribution

| Component   | percentage |
| ----------- | ---------- |
| Minor       | 30         |
| Major       | 35         |
| Assignments | 10         |
| Projects    | 25         |


Grading will be relative while ensuring absolute outer bounds (Above 80% is necessary but not sufficient for A). You fail if you are below 25%. Both attendance will be taken. Less than 40% attendance will lead to grade drop. Those who have 85% attendance (sick leaves with proof from campus hospital excluded) will get a grade increase, if they are by within 5 ranks. Attendance includes lab attendance too. In labs, the tutor may ask to randomly switch on cameras to ensure you are present. 
## Ethics Policy
* If you copy in exams or in assignment, from each identified cluster of similar answers, (only) one person will be penalised. The discretion of choosing this person lies with us, and usually we will pick the person who scored the most marks in the course until that point.  
* Codes you submit will go through smart plagiarism checks. So code copying will be dealt with very seriously. 
* Penalty for copying/plagiarism of code is drop in grade. If you are caught multiple times, grades drop multiple times.	

What to do in probablity applications in engineering math.  *Introduction to probability models by Sheldon Ross* - squeeze into 5%
## Reference Books 
Linear algebra: Bowen and Wang "Introduction to Tensors and Vectors - Part 1" [@bowen2008introduction], Greenberg "Engineering Mathematics ", Gilbert Strang "Linear Algebra and learning from data"

Greenberg will be the course text book for most of the course. 

The other text books will be updated before the Chapters and will be mentioned in the course notes. 
