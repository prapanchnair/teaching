BUILDDIR=build
FILENAME=Ch01_linear_algebra
#pdf:
#    mkdir $(BUILDDIR) -p
#    pandoc $(FILENAME).md \
#    --citeproc \
#    --from=markdown+tex_math_single_backslash+tex_math_dollars+raw_tex \
#    --to=latex \
#    --output=$(BUILDDIR)/$(FILENAME).pdf \
#    --pdf-engine=pdflatex
pdf:    
	mkdir $(BUILDDIR) -p
	pandoc $(FILENAME).md \
	--filter pandoc-citeproc \
	--from=markdown+tex_math_single_backslash+tex_math_dollars+raw_tex \
	--to=latex \
	--output=$(BUILDDIR)/$(FILENAME).pdf
